\echo -- Numeric Types

\echo -- Integer Types
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t (
	s smallint,   -- 2 bytes
    i integer,    -- 4 bytes
    b bigint      -- 8 bytes
);

\echo -- Arbitrary precision
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t (
    t1 NUMERIC,      -- Any number
    t2 NUMERIC(5),   -- 5 significant digits
    t3 NUMERIC(5,2) -- 5 significant digits, 2 decimal points
);

\echo -- Floating point
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t (
    r real,
    d double precision
);

\echo -- Serial == auto-incrementing integer
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t (
	s SERIAL,
	i INT
);
INSERT INTO t(i) VALUES (100),(10);
SELECT * FROM t;
INSERT INTO t(i) VALUES (1) RETURNING s;
SELECT currval(pg_get_serial_sequence('t', 's'));
\d t
table t;
SELECT * FROM information_schema.sequences;

\echo -- Monetary
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t (
	m money    -- 8 bytes , 2 digits
);
INSERT INTO t VALUES (12345.6789);
SELECT * FROM t;

\echo -- Character types
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t (
    t text,        -- Unlimited length
    c char(3),     -- Fixed-length
    v varchar(3)   -- Variable-length with limit
);

\echo -- Byte array
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t (
    ba bytea
);
INSERT INTO t(ba) VALUES ('\xDEADBEEF');

\echo -- Date Time
SHOW TIMEZONE;
SET timezone='Europe/Brussels';

DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
	ts1 TIMESTAMP,
	ts2 TIMESTAMPTZ,
    d date,
    t1 time WITHOUT TIME ZONE,
    t2 time WITH TIME ZONE
);
INSERT INTO t(ts1) VALUES('2000-12-21 23:59:59');
INSERT INTO t(ts2) VALUES('2000-12-21 23:59:59+01');
INSERT INTO t(d)   VALUES('2000-12-21');
INSERT INTO t(t1)  VALUES('23:59:59');
INSERT INTO t(t2)  VALUES('23:59:59+01');

\echo -- Boolean
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t (
	b BOOLEAN
);
INSERT INTO t(b) VALUES (true),(false);

\echo -- Enum
DROP TYPE IF EXISTS mood;
CREATE TYPE mood AS ENUM ('sad','ok','happy');

\echo -- Bit String
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t (
    a BIT(3),                --Fixed length
    b BIT VARYING(5) 
);
INSERT INTO t(a,b) VALUES (B'101', B'00');

\echo -- xml
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t (
    p int, 
    q xml
);
INSERT INTO t VALUES (1, XMLPARSE (CONTENT 'abc<foo>bar</foo><bar>foo</bar>'));
SELECT xmlagg(q) FROM t;

\echo -- Json
SELECT '5'::json;
SELECT '[1, 2, "foo", null]'::json;
SELECT '{"bar": "baz", "balance": 7.77, "active": false}'::json;
SELECT '{"foo": [true, "bar"], "tags": {"a": 1, "b": null}}'::json;
SELECT '{"bar": "baz", "balance": 7.77, "active":false}'::json;

DROP TABLE IF EXISTS tj CASCADE;
CREATE TABLE tj(
    j JSON
);
INSERT INTO tj(j) VALUES 
    (
            '{ "customer": "John Doe", 
                "items": {"product": "Beer",
                                 "qty": 6
                               }
             }'
     );
SELECT * FROM tj;
\echo json-format
SELECT j->'customer' AS customer FROM tj;
\echo text-format
SELECT j->>'customer' AS customer FROM tj;
\echo json as json-kv-pairs
SELECT JSON_EACH(j) FROM tj;
\echo json as text-kv-pairs
SELECT JSON_EACH_TEXT(j) FROM tj;

DROP TABLE IF EXISTS myuser CASCADE;
CREATE table myuser(
	id integer primary key,
	email varchar not null,
	name varchar not null,
	password_digest varchar not null
);
INSERT INTO myuser values
	(1,'alain@yahoo.com','Alain','123456'),
	(2,'piet@alef.com','Piet','56789');
\echo show 
TABLE myuser;
SELECT row_to_json(myuser) FROM myuser WHERE id=1;
\echo hide password
SELECT row_to_json(row(id,name,email)) FROM myuser WHERE id=1;
SELECT row_to_json(t) FROM
	(
	SELECT id,name,email FROM myuser
	WHERE id=1
	)
		AS t;
\echo -- Containment
SELECT '"foo"'::jsonb @> '"foo"'::jsonb;
SELECT '[1, 2, 3]'::jsonb @> '[1, 3]'::jsonb;
SELECT '[1, 2, 3]'::jsonb @> '[3, 1]'::jsonb;
SELECT '[1, 2, 3]'::jsonb @> '[1, 2, 2]'::jsonb;
SELECT '{"product": "PostgreSQL", "version": 9.4, "jsonb":true}'::jsonb 
    @> '{"version": 9.4}'::jsonb;
SELECT '["foo", "bar"]'::jsonb @> '"bar"'::jsonb;
\echo --Existence
SELECT '["foo", "bar", "baz"]'::jsonb ? 'bar';
SELECT '{"foo": "bar"}'::jsonb ? 'foo';
SELECT '"foo"'::jsonb ? 'foo';

\echo -- Jsonb
SELECT json_agg(row_to_json(myuser)) FROM myuser;

\echo -- ???
DROP TABLE IF EXISTS tjb CASCADE;
CREATE TABLE tjb(
    jbook jsonb
);
INSERT INTO tjb(jbook) VALUES 
        (
            '{"title": "Sleeping Beauties", 
               "genres": ["Fiction", "Thriller", "Horror"], 
               "published": false
             }'
        );
SELECT jbook->'title' AS title FROM tjb;
SELECT * FROM tjb WHERE jbook->'published' = 'false';
\echo expand
SELECT jsonb_array_elements_text(jbook->'genres') AS genre  FROM tjb;
\echo containment check
SELECT '["Fiction", "Thriller", "Horror"]'::jsonb @> '["Fiction", "Horror"]'::jsonb;  
SELECT jbook->'title' FROM tjb WHERE jbook->'genres' @> '["Fiction"]'::jsonb;
\echo existence
SELECT COUNT(*) FROM tjb WHERE jbook ? 'genres'; 

\echo -- Array
SELECT ('{red,green,blue}'::text[])[1] ;
SELECT ('{1,2,3}'::int[])[1] ;
SELECT ARRAY (VALUES (1),(2));

DROP TABLE IF EXISTS arr CASCADE;
CREATE TABLE arr(
    workday int[]
);
INSERT INTO arr VALUES('{0,1,2}');
INSERT INTO arr VALUES('{3,4}');
SELECT * from arr;
SELECT workday[3] from arr;

DROP TABLE IF EXISTS m CASCADE;
CREATE TABLE m(
    id SERIAL PRIMARY KEY,
    a int[]
);
INSERT INTO m(a) VALUES ('{0,1,2}'::int[]);
INSERT INTO m(a) VALUES ('{2,3,4}'::int[]);
SELECT a [1:2] FROM m;
\echo Concat
UPDATE m set a = array_append(a,5);
SELECT a || 6 AS "b" FROM m;

DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    x SERIAL,
    i int[]);
INSERT INTO t(i) VALUES('{1,2}');
INSERT INTO t(i) VALUES('{3,4}');
select i from t;
select i[1] from t;
select unnest(i) from t;
SELECT * FROM t where i[1] IN ('1','3');
SELECT * FROM t where i @> ARRAY[1,2];
UPDATE t set i[1]=5;
SELECT * FROM t;

SELECT array_prepend(1, ARRAY[2,3]);
SELECT array_append(ARRAY[1,2], 3);
SELECT array_cat(ARRAY[1,2], ARRAY[3,4]);

\echo -- Composite types, row,record
DROP TYPE IF EXISTS complex;
CREATE TYPE complex AS(
    r double precision,
    i double precision
);
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    c complex
);
INSERT INTO t(c) VALUES (ROW(1.0,2.0));

\echo -- Range types
select int4range(1,5);
\echo inclusion operator @>
select int4range(1,5) @>3;
\echo overlap operator &&
select int4range(1,5) && int4range(3,7);
\echo lower end inclusive
select int4range(1,5) @>1;
\echo upper end exclusive
select int4range(1,5) @>5;
\echo square brackets ==inclusive
select int4range(1,5,'[]') @>5;
SELECT '[3,7)'::int4range; --includes 3
SELECT '(3,7]'::int4range; --includes 7

\echo -- Domain types
DROP DOMAIN IF EXISTS posint;
CREATE DOMAIN posint AS integer CHECK (VALUE > 0);

\echo -- Cleanup
DROP TABLE IF EXISTS arr CASCADE;
DROP TABLE IF EXISTS m CASCADE;
DROP TABLE IF EXISTS myuser CASCADE;
DROP TABLE IF EXISTS t CASCADE;
DROP TABLE IF EXISTS tj CASCADE;
DROP TABLE IF EXISTS tjb CASCADE;
DROP DOMAIN IF EXISTS posint;
DROP TYPE IF EXISTS complex;
DROP TYPE IF EXISTS mood;

