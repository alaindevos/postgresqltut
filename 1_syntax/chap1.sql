-- \h CREATE TABLE
\c x

\echo --Cleanup
DROP VIEW IF EXISTS myview;
DROP TABLE IF EXISTS accounts CASCADE;
DROP TABLE IF EXISTS arr CASCADE;
DROP TABLE IF EXISTS capitals CASCADE;
DROP TABLE IF EXISTS cities CASCADE;
DROP TABLE IF EXISTS empsalary CASCADE;
DROP TABLE IF EXISTS weather CASCADE;
DROP FUNCTION IF EXISTS concat_lower_or_upper(a text, b text, uppercase boolean);

\echo --Intro
select version();
select current_date;
select 2+2;
CREATE TABLE weather(
    city varchar(80),
    temp_lo int,
    temp_hi int,
    prcp    real,
    date    date
);
\echo -- create table
CREATE TABLE cities(
    name varchar(80),
    location point    
);
\echo -- populate table
INSERT INTO weather VALUES ('San Francisco', 46, 50, 0.25,'1994-11-27');
INSERT INTO cities VALUES ('San Francisco', '(-194.0, 53.0)');
INSERT INTO weather (city, temp_lo, temp_hi, prcp, date) VALUES ('San Francisco2', 43, 57, 0.0, '1994-11-29');
INSERT INTO weather (date, city, temp_hi, temp_lo) VALUES ('1994-11-29', 'Hayward', 54, 37);
\echo -- query table
SELECT * FROM weather;
SELECT city, temp_lo, temp_hi, prcp, date FROM weather;
SELECT city, (temp_hi+temp_lo)/2 AS temp_avg, date FROM weather;
SELECT * FROM weather
    WHERE city = 'San Francisco' AND prcp > 0.0;
SELECT * FROM weather
    ORDER BY city,temp_lo;
SELECT DISTINCT city
    FROM weather;
\echo -- join table
SELECT * FROM weather 
    JOIN cities
        ON city=name;
SELECT weather.city, weather.temp_lo, weather.temp_hi, weather.prcp, weather.date, cities.location FROM weather 
    JOIN cities 
        ON weather.city = cities.name;
SELECT * FROM weather 
    LEFT OUTER JOIN cities 
        ON weather.city = cities.name;
SELECT w1.city, w1.temp_lo AS low, w1.temp_hi AS high, w2.city, w2.temp_lo AS low, w2.temp_hi AS high FROM weather w1 
    JOIN weather w2
        ON w1.temp_lo < w2.temp_lo AND w1.temp_hi > w2.temp_hi;
SELECT * FROM weather w 
    JOIN cities c 
        ON w.city = c.name;
\echo --aggregate functions
SELECT max(temp_lo) FROM weather;
\echo --subquery
SELECT city FROM weather
    WHERE temp_lo = (SELECT max(temp_lo) FROM weather);
\echo -- group by
SELECT city, max(temp_lo) FROM weather
    GROUP BY city;
\echo -- having
SELECT city, max(temp_lo) FROM weather
    GROUP BY city
        HAVING max(temp_lo) < 40;
\echo --like
SELECT city, max(temp_lo) FROM weather
    WHERE city LIKE 'H%'
        GROUP BY city
            HAVING max(temp_lo) < 40;
\echo --update
SELECT * from weather;
UPDATE weather
    SET temp_hi = temp_hi - 2,temp_lo = temp_lo - 2
        WHERE date > '1994-11-28';
SELECT * from weather;
\echo --delete
DELETE FROM weather WHERE city = 'Hayward';
\echo --weather
SELECT * from weather;
\echo --cities
SELECT * from cities;
\echo --view
CREATE VIEW myview AS
    SELECT name, temp_lo, temp_hi, prcp, date, location
        FROM weather, cities
            WHERE city = name;
SELECT * FROM myview;
DROP TABLE IF EXISTS  cities CASCADE;
DROP TABLE IF EXISTS  weather CASCADE;
\echo --foreign key
CREATE TABLE cities (
    name varchar(80) primary key,
    location point
);
CREATE TABLE weather (
    city varchar(80) references cities(name),
    temp_lo int,
    temp_hi int,
    prcp real,
    date date
);
\echo --constraint error
INSERT INTO weather VALUES ('Berkeley', 45, 53, 0.0, '1994-11-28');
\echo --transactions
CREATE TABLE accounts (
    name varchar(50),
    balance real
);
INSERT INTO accounts VALUES ('Alice',300.0);
INSERT INTO accounts VALUES ('Bob',200.0);
\echo --transaction
BEGIN;
SAVEPOINT my_savepoint;
UPDATE accounts SET balance = balance - 100.0 WHERE name ='Alice';
UPDATE accounts SET balance = balance + 100.0 WHERE name ='Bob';
ROLLBACK TO my_savepoint;
COMMIT;
\echo --window function
CREATE TABLE empsalary(
    depname varchar(50),
    empno   int,
    salary  real,
    rank int
);
INSERT INTO empsalary VALUES ('develop',11,5200,1);
INSERT INTO empsalary VALUES ('develop',7,4200,2);
INSERT INTO empsalary VALUES ('develop',9,4500,2);
INSERT INTO empsalary VALUES ('develop',8,6000,4);
INSERT INTO empsalary VALUES ('develop',10,5200,5);
INSERT INTO empsalary VALUES ('personnel',5,3500,1);
INSERT INTO empsalary VALUES ('personnel',2,3900,2);
INSERT INTO empsalary VALUES ('sales',3,4800,1);
INSERT INTO empsalary VALUES ('sales',1,5000,2);
INSERT INTO empsalary VALUES ('sales',4,4800,2);
SELECT depname, empno, salary, avg(salary) 
    OVER (PARTITION BY depname) 
        FROM empsalary;
SELECT depname, empno, salary, rank() 
    OVER (PARTITION BY depname ORDER BY salary DESC)
        FROM empsalary;
SELECT salary,sum(salary) OVER () FROM empsalary;
SELECT salary, sum(salary) OVER (ORDER BY salary) FROM empsalary;
\echo -- rank < 3
SELECT depname, empno, salary FROM    
    (SELECT depname, empno, salary, rank() 
        OVER (PARTITION BY depname ORDER BY salary DESC,empno) AS pos
            FROM empsalary
    ) AS ss
        WHERE pos < 3;
SELECT sum(salary) OVER w, avg(salary) OVER w FROM empsalary
    WINDOW w AS 
        (PARTITION BY depname ORDER BY salary DESC);
\echo --inheritence
DROP TABLE IF EXISTS  cities CASCADE;
CREATE TABLE cities(
    name text,
    population real,
    elevation int
);
CREATE TABLE capitals (
    state char(2) UNIQUE NOT NULL
) INHERITS (cities);
INSERT INTO cities(name,population,elevation) VALUES ('Las Vegas',1,2174);
INSERT INTO cities(name,population,elevation) VALUES ('Mariposa',2,1953);
INSERT INTO capitals(state,name,population,elevation) VALUES ('MA','Madison',3,845);
SELECT name,elevation 
    FROM cities
        WHERE elevation > 500;
\echo -- non-capitals
SELECT name, elevation
    FROM ONLY cities
        WHERE elevation > 500;
\echo -- Comment
-- Comment
\echo --Multiline Comment
/*  1
*   2
*   3
*/
\echo -- String Constant
SELECT 'foo';
\echo -- Dollar quote
SELECT $$foo$$;
\echo -- Array
SELECT ARRAY[1,2,3+4];
\echo -- Cast
SELECT ARRAY[1,2,22.7]::integer[];
\echo -- Multidimensional array
SELECT ARRAY[ARRAY[1,2],ARRAY[3,4]];
CREATE TABLE arr(
    f1  int[],
    f2  int[]
);
INSERT INTO arr VALUES (ARRAY[[1,2],[3,4]], ARRAY[[5,6],[7,8]]);
SELECT ARRAY[f1, f2, '{{9,10},{11,12}}'::int[]] FROM arr;
SELECT ARRAY(SELECT ARRAY[i, i*2] 
    FROM generate_series(1,5) AS a(i));
\echo -- Row Constructor
SELECT ROW(1,2.5,'this is a test');
\echo -- Function
CREATE FUNCTION         concat_lower_or_upper(a text, b text, uppercase boolean DEFAULT false)   RETURNS text
    AS $$
        SELECT CASE
            WHEN $3 THEN UPPER($1 || ' ' || $2)
            ELSE LOWER($1 || ' ' || $2)
        END;
        $$
LANGUAGE SQL IMMUTABLE STRICT;
\echo -- Function Calling
SELECT concat_lower_or_upper('Hello', 'World', true);
\echo -- Named Notation
SELECT concat_lower_or_upper(a => 'Hello', b => 'World');
SELECT concat_lower_or_upper(a => 'Hello', b => 'World', uppercase => true);
SELECT concat_lower_or_upper(a => 'Hello', uppercase => true, b => 'World');
\echo -- Older Syntax
SELECT concat_lower_or_upper(a := 'Hello', uppercase := true, b :='World');

\echo -- Cleanup
DROP VIEW IF EXISTS myview;
DROP TABLE IF EXISTS accounts CASCADE;
DROP TABLE IF EXISTS arr CASCADE;
DROP TABLE IF EXISTS capitals CASCADE;
DROP TABLE IF EXISTS cities CASCADE;
DROP TABLE IF EXISTS empsalary CASCADE;
DROP TABLE IF EXISTS weather CASCADE;
DROP FUNCTION IF EXISTS concat_lower_or_upper(a text, b text, uppercase boolean);
\q
