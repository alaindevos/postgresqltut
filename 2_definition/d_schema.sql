\echo -- Schema
\c x
DROP SCHEMA IF EXISTS myschema CASCADE;
CREATE SCHEMA   myschema;
DROP TABLE IF EXISTS x.myschema.mytable CASCADE;
CREATE TABLE         x.myschema.mytable (
    i integer    
);
SELECT * FROM x.myschema.mytable;
DROP TABLE IF EXISTS myschema.mytable CASCADE;
CREATE TABLE         myschema.mytable (
    i integer    
);
SELECT * FROM myschema.mytable;
DROP   SCHEMA myschema CASCADE;
CREATE SCHEMA myschema AUTHORIZATION x;
\echo -- Public Schema
DROP TABLE IF EXISTS mytable CASCADE;
CREATE TABLE    mytable (
    i integer    
);
DROP TABLE IF EXISTS mytable CASCADE;
CREATE TABLE    public.mytable (
    i integer    
);
\echo -- Show Schema Search Path
SHOW search_path;
SET search_path TO "$user", public,myschema;
\echo -- System Catalog schema
select nspname from pg_catalog.pg_namespace;
\echo -- Cleanup
DROP SCHEMA     myschema CASCADE;
DROP TABLE IF EXISTS mytable CASCADE;

