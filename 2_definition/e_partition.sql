\echo -- Declarative Partitioning

DROP TABLE IF EXISTS measurement          CASCADE;
DROP TABLE IF EXISTS measurement_y2006m02 CASCADE;
DROP TABLE IF EXISTS measurement_y2007m01 CASCADE;
DROP TABLE IF EXISTS measurement_y2008m01 CASCADE;
DROP TABLE IF EXISTS measurement_y2009m01 CASCADE;
DROP TABLE IF EXISTS measurement_y2010m01 CASCADE;

CREATE TABLE measurement (
    city_id
    int not null,
    logdate date not null,
    peaktemp int,
    unitsales int
  ) 
  PARTITION BY RANGE (logdate);
CREATE INDEX ON measurement (logdate);
CREATE TABLE measurement_y2006m01 PARTITION OF measurement
    FOR VALUES FROM ('2006-01-01') TO ('2006-02-01');
CREATE TABLE measurement_y2006m02 PARTITION OF measurement
    FOR VALUES FROM ('2006-02-01') TO ('2006-03-01');
\echo -- Tablespace
DROP TABLESPACE IF EXISTS fasttablespace;
CREATE TABLESPACE fasttablespace LOCATION '/var/db/postgres/data12/fasttablespace';
CREATE TABLE measurement_y2007m01 PARTITION OF measurement
    FOR VALUES FROM ('2007-01-01') TO ('2007-02-01')
        TABLESPACE fasttablespace;
CREATE TABLE measurement_y2008m01 PARTITION OF measurement
    FOR VALUES FROM ('2008-01-01') TO ('2008-02-01')
        WITH (parallel_workers = 4)
            TABLESPACE fasttablespace;
\echo -- SubPartition
CREATE TABLE measurement_y2009m01 PARTITION OF measurement
    FOR VALUES FROM ('2009-01-01') TO ('2009-02-01')
        PARTITION BY RANGE (peaktemp);
\echo -- Delete Partitions
ALTER TABLE measurement DETACH PARTITION measurement_y2007m01;
\echo -- Create Table Like
CREATE TABLE measurement_y2010m01
    (LIKE measurement INCLUDING DEFAULTS INCLUDING CONSTRAINTS)
        TABLESPACE fasttablespace;
ALTER TABLE  measurement_y2010m01 ADD CONSTRAINT y2010m02
    CHECK ( logdate >= DATE '2010-01-01' AND logdate < DATE '2010-02-01' );
ALTER TABLE  measurement ATTACH PARTITION measurement_y2010m01
    FOR VALUES FROM ('2010-01-01') TO ('2010-02-01' );
\echo -- Attach indexes
CREATE INDEX measurement_usls_idx ON ONLY measurement (unitsales);
CREATE INDEX measurement_usls_2010_idx
    ON measurement_y2010m01 (unitsales);
ALTER INDEX measurement_usls_idx
    ATTACH PARTITION 
             measurement_usls_2010_idx;
ALTER TABLE ONLY measurement ADD UNIQUE (city_id, logdate);
ALTER TABLE measurement_y2010m01 ADD UNIQUE (city_id, logdate);
\echo -- Pruning
SET enable_partition_pruning = on;
SELECT count(*) FROM measurement WHERE logdate >= DATE '2008-01-01';
\echo -- Cleanup
DROP TABLE IF EXISTS measurement          CASCADE;
DROP TABLE IF EXISTS measurement_y2006m02 CASCADE;
DROP TABLE IF EXISTS measurement_y2007m01 CASCADE;
DROP TABLE IF EXISTS measurement_y2008m01 CASCADE;
DROP TABLE IF EXISTS measurement_y2009m01 CASCADE;
DROP TABLE IF EXISTS measurement_y2010m01 CASCADE;

