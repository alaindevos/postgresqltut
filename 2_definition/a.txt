Null display is "[NULL]".
Border style is 2.
Line style is unicode.
Unicode border line style is "double".
Unicode header line style is "double".
Unicode column line style is "single".
Output format is wrapped.
Expanded display is used automatically.
--Tables introduction
DROP TABLE IF EXISTS t CASCADE;
DROP TABLE
CREATE TABLE T (
    i INT
);
CREATE TABLE
DROP TABLE IF EXISTS myuser CASCADE;
DROP TABLE
CREATE TABLE myuser(
  id SERIAL PRIMARY KEY ,
  first_name VARCHAR NOT NULL,
  last_name VARCHAR NOT NULL,
  user_name VARCHAR NOT NULL,
  active BOOLEAN DEFAULT TRUE,
  CONSTRAINT unique_name_user_name UNIQUE (first_name, last_name, user_name)
);
CREATE TABLE
--Drop Table
DROP TABLE IF EXISTS my_first_table CASCADE;
DROP TABLE
CREATE TABLE my_first_table (
    first_column text,
    second_column integer
);
CREATE TABLE
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
CREATE TABLE products(
    product_no integer,
    name text,
    price numeric
);
CREATE TABLE
--Default Value
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
CREATE TABLE products(
    product_no integer,
    name text,
    price numeric DEFAULT 9.99
);
CREATE TABLE
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
--Serial
CREATE TABLE products(
    product_no serial,
    name text,
    price numeric DEFAULT 9.99
);
CREATE TABLE
-- Generated Column
DROP TABLE IF EXISTS people CASCADE;
DROP TABLE
CREATE TABLE people (
    height_cm numeric,
    height_in numeric GENERATED ALWAYS AS (height_cm / 2.54) STORED
);
CREATE TABLE
-- Check Constraint
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
CREATE TABLE products (
    product_no integer,
    name text,
    price numeric,
    CHECK (price > 0)
);
CREATE TABLE
-- Named Check Constraint
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
CREATE TABLE products (
    product_no integer,
    name text,
    price numeric,
    CONSTRAINT constraint_positive_price CHECK (price > 0)
);
CREATE TABLE
-- Not-Null Constraint
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
CREATE TABLE products (
    product_no integer NOT NULL,
    name text NOT NULL,
    price numeric
);
CREATE TABLE
-- Unique Constraint
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
CREATE TABLE products (
    product_no integer UNIQUE,
    name text,
    price numeric
);
CREATE TABLE
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
CREATE TABLE products (
    product_no integer,
    name text,
    price numeric,
    UNIQUE (product_no)
);
CREATE TABLE
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
CREATE TABLE products (
    product_no integer CONSTRAINT constraint_unique UNIQUE,
    name text,
    price numeric
);
CREATE TABLE
-- Primary keys (unique and not null)
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
CREATE TABLE products (
    product_no integer PRIMARY KEY,
    name text,
    price numeric
);
CREATE TABLE
DROP TABLE IF EXISTS example CASCADE;
DROP TABLE
CREATE TABLE example (
    a integer,
    b integer,
    c integer,
    PRIMARY KEY (a, c)
);
CREATE TABLE
--Foreign key Intro 1
DROP TABLE IF EXISTS car CASCADE;
DROP TABLE
DROP TABLE IF EXISTS owner CASCADE;
DROP TABLE
CREATE TABLE owner(
    id serial UNIQUE,
    name varchar
);
CREATE TABLE
CREATE TABLE car(
    model varchar,
    owner_id integer,
    FOREIGN KEY (owner_id) REFERENCES owner(id)
);
CREATE TABLE
INSERT INTO owner(name) VALUES ('Alain');
INSERT 0 1
INSERT INTO owner(name) VALUES ('Eddy');
INSERT 0 1
INSERT INTO car(model,owner_id) VALUES ('Ford',(SELECT id FROM owner WHERE name='Alain'));
INSERT 0 1
--Foreign key Intro 2
DROP TABLE IF EXISTS person CASCADE;
DROP TABLE
CREATE TABLE person(
  id SERIAL,
  emp_id int,
  name VARCHAR(100),
  
  PRIMARY KEY (id),
  UNIQUE(id)
);
CREATE TABLE
INSERT INTO person(emp_id,name) VALUES (1,'Alain');
INSERT 0 1
INSERT INTO person(emp_id,name) VALUES (2,'Eddy');
INSERT 0 1
INSERT INTO person(emp_id,name) VALUES (3,'Alain');
INSERT 0 1
DROP TABLE IF EXISTS phone CASCADE;
DROP TABLE
CREATE TABLE phone (
  id SERIAL,
  emp_id int,
  type char(3),
  number bigint,
  ext int,
  
  PRIMARY KEY (id),
  UNIQUE(id),
  FOREIGN KEY(emp_id) REFERENCES person(id)
);
CREATE TABLE
INSERT INTO phone(emp_id,type,number,ext)
  VALUES (1,'wrk',11111111,23);
INSERT 0 1
INSERT INTO phone(emp_id,type,number,ext)
  VALUES (1,'hom',22222222,NULL);
INSERT 0 1
INSERT INTO phone(emp_id,type,number,ext)
  VALUES (2,'wrk',33333333,45);
INSERT 0 1
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
CREATE TABLE products (
    product_no integer PRIMARY KEY,
    name text,
    price numeric
);
CREATE TABLE
DROP TABLE IF EXISTS orders CASCADE;
DROP TABLE
CREATE TABLE orders (
    order_id integer PRIMARY KEY,
    product_no integer REFERENCES products (product_no),
    quantity integer
);
CREATE TABLE
DROP TABLE IF EXISTS orders CASCADE;
DROP TABLE
CREATE TABLE orders (
    order_id integer PRIMARY KEY,
    product_no integer REFERENCES products,
    quantity integer
);
CREATE TABLE
DROP TABLE IF EXISTS t1 CASCADE;
DROP TABLE
CREATE TABLE t1 (
    a integer,
    b integer,
    c integer,
    PRIMARY KEY (a, b),
    FOREIGN KEY (b, c) REFERENCES t1 (a, b)
);
CREATE TABLE
DROP TABLE IF EXISTS tree CASCADE;
DROP TABLE
CREATE TABLE tree (
    node_id integer PRIMARY KEY,
    parent_id integer REFERENCES tree(node_id),
    name text
);
CREATE TABLE
--Multiple Foreign Keys
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
CREATE TABLE products (
    product_no integer PRIMARY KEY,
    name text,
    price numeric
);
CREATE TABLE
DROP TABLE IF EXISTS orders CASCADE;
DROP TABLE
CREATE TABLE orders (
    order_id integer PRIMARY KEY,
    shipping_address text
);
CREATE TABLE
DROP TABLE IF EXISTS order_items CASCADE;
DROP TABLE
CREATE TABLE order_items (
    product_no integer REFERENCES products,
    order_id integer REFERENCES orders,
    quantity integer,
    PRIMARY KEY (product_no, order_id)
);
CREATE TABLE
--Many-to-many relationship
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
CREATE TABLE products (
    product_no integer PRIMARY KEY,
    name text,
    price numeric
);
CREATE TABLE
DROP TABLE IF EXISTS orders CASCADE;
DROP TABLE
CREATE TABLE orders (
    order_id integer PRIMARY KEY,
    shipping_address text
);
CREATE TABLE
DROP TABLE IF EXISTS order_items CASCADE;
DROP TABLE
CREATE TABLE order_items (
    product_no integer REFERENCES products ON DELETE RESTRICT,
    order_id integer REFERENCES orders ON DELETE CASCADE,
    quantity integer,
    PRIMARY KEY (product_no, order_id)
);
CREATE TABLE
--System-columns
DROP TABLE IF EXISTS t CASCADE;
DROP TABLE
CREATE TABLE t(
    name varchar(20)
);
CREATE TABLE
INSERT INTO t VALUES ('Alain'),('Eddy');
INSERT 0 2
SELECT tableoid,ctid FROM t;
╔══════════╤═══════╗
║ tableoid │ ctid  ║
╠══════════╪═══════╣
║   140441 │ (0,1) ║
║   140441 │ (0,2) ║
╚══════════╧═══════╝
(2 rows)

--Cleanup
DROP TABLE IF EXISTS myuser CASCADE;
DROP TABLE
DROP TABLE IF EXISTS my_first_table CASCADE;
DROP TABLE
DROP TABLE IF EXISTS people CASCADE;
DROP TABLE
DROP TABLE IF EXISTS example CASCADE;
DROP TABLE
DROP TABLE IF EXISTS person CASCADE;
DROP TABLE
DROP TABLE IF EXISTS owner CASCADE;
DROP TABLE
DROP TABLE IF EXISTS car CASCADE;
DROP TABLE
DROP TABLE IF EXISTS phone CASCADE;
DROP TABLE
DROP TABLE IF EXISTS t1 CASCADE;
DROP TABLE
DROP TABLE IF EXISTS tree CASCADE;
DROP TABLE
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE
DROP TABLE IF EXISTS order_items CASCADE;
DROP TABLE
DROP TABLE IF EXISTS orders CASCADE;
DROP TABLE
DROP TABLE IF EXISTS t CASCADE;
DROP TABLE
