DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products (
    product_no integer,
    name text,
    price numeric
);
\echo -- Adding Column
ALTER TABLE products ADD COLUMN description text;
\echo -- Removing Column
ALTER TABLE products DROP COLUMN description;
\echo -- Adding Constraint
DROP TABLE IF EXISTS product_groups CASCADE;
CREATE TABLE product_groups(
    product_groups integer PRIMARY KEY
);
ALTER TABLE products ADD CHECK (name <> '');
ALTER TABLE products ADD CONSTRAINT some_name UNIQUE (product_no);
ALTER TABLE products ADD COLUMN product_group_id integer;
ALTER TABLE products ADD FOREIGN KEY (product_group_id) REFERENCES product_groups;
\echo -- Removing Constraint
ALTER TABLE products DROP CONSTRAINT some_name;
\echo -- Add & Remove Not-Null Constraint
ALTER TABLE products ALTER  COLUMN product_no SET NOT NULL;
ALTER TABLE products ALTER  COLUMN product_no DROP NOT NULL;
\echo -- Change Default Value
ALTER TABLE products ALTER COLUMN price SET DEFAULT 7.77;
ALTER TABLE products ALTER COLUMN price DROP DEFAULT;
\echo -- Change Data Type
ALTER TABLE products ALTER COLUMN price TYPE numeric(10,2);
\echo -- Rename Column
ALTER TABLE products RENAME COLUMN product_no TO product_number;
\echo -- Rename Table
DROP TABLE IF EXISTS items2 CASCADE;
ALTER TABLE products RENAME TO items2;
\echo -- Cleanup
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE IF EXISTS product_groups CASCADE;
DROP TABLE IF EXISTS items CASCADE;
DROP TABLE IF EXISTS items2 CASCADE;

