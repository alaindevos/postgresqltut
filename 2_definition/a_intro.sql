\echo --Tables introduction
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE T (
    i INT
);
DROP TABLE IF EXISTS myuser CASCADE;
CREATE TABLE myuser(
  id SERIAL PRIMARY KEY ,
  first_name VARCHAR NOT NULL,
  last_name VARCHAR NOT NULL,
  user_name VARCHAR NOT NULL,
  active BOOLEAN DEFAULT TRUE,
  CONSTRAINT unique_name_user_name UNIQUE (first_name, last_name, user_name)
);
\echo --Drop Table
DROP TABLE IF EXISTS my_first_table CASCADE;
CREATE TABLE my_first_table (
    first_column text,
    second_column integer
);
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products(
    product_no integer,
    name text,
    price numeric
);
\echo --Default Value
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products(
    product_no integer,
    name text,
    price numeric DEFAULT 9.99
);
DROP TABLE IF EXISTS products CASCADE;
\echo --Serial
CREATE TABLE products(
    product_no serial,
    name text,
    price numeric DEFAULT 9.99
);
\echo -- Generated Column
DROP TABLE IF EXISTS people CASCADE;
CREATE TABLE people (
    height_cm numeric,
    height_in numeric GENERATED ALWAYS AS (height_cm / 2.54) STORED
);
\echo -- Check Constraint
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products (
    product_no integer,
    name text,
    price numeric,
    CHECK (price > 0)
);
\echo -- Named Check Constraint
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products (
    product_no integer,
    name text,
    price numeric,
    CONSTRAINT constraint_positive_price CHECK (price > 0)
);
\echo -- Not-Null Constraint
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products (
    product_no integer NOT NULL,
    name text NOT NULL,
    price numeric
);
\echo -- Unique Constraint
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products (
    product_no integer UNIQUE,
    name text,
    price numeric
);
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products (
    product_no integer,
    name text,
    price numeric,
    UNIQUE (product_no)
);
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products (
    product_no integer CONSTRAINT constraint_unique UNIQUE,
    name text,
    price numeric
);
\echo -- Primary keys (unique and not null)
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products (
    product_no integer PRIMARY KEY,
    name text,
    price numeric
);
DROP TABLE IF EXISTS example CASCADE;
CREATE TABLE example (
    a integer,
    b integer,
    c integer,
    PRIMARY KEY (a, c)
);
\echo --Foreign key Intro 1
DROP TABLE IF EXISTS car CASCADE;
DROP TABLE IF EXISTS owner CASCADE;
CREATE TABLE owner(
    id serial UNIQUE,
    name varchar
);
CREATE TABLE car(
    model varchar,
    owner_id integer,
    FOREIGN KEY (owner_id) REFERENCES owner(id)
);
INSERT INTO owner(name) VALUES ('Alain');
INSERT INTO owner(name) VALUES ('Eddy');
INSERT INTO car(model,owner_id) VALUES ('Ford',(SELECT id FROM owner WHERE name='Alain'));
\echo --Foreign key Intro 2
DROP TABLE IF EXISTS person CASCADE;
CREATE TABLE person(
  id SERIAL,
  emp_id int,
  name VARCHAR(100),
  --
  PRIMARY KEY (id),
  UNIQUE(id)
);
INSERT INTO person(emp_id,name) VALUES (1,'Alain');
INSERT INTO person(emp_id,name) VALUES (2,'Eddy');
INSERT INTO person(emp_id,name) VALUES (3,'Alain');
DROP TABLE IF EXISTS phone CASCADE;
CREATE TABLE phone (
  id SERIAL,
  emp_id int,
  type char(3),
  number bigint,
  ext int,
  --
  PRIMARY KEY (id),
  UNIQUE(id),
  FOREIGN KEY(emp_id) REFERENCES person(id)
);
INSERT INTO phone(emp_id,type,number,ext)
  VALUES (1,'wrk',11111111,23);
INSERT INTO phone(emp_id,type,number,ext)
  VALUES (1,'hom',22222222,NULL);
INSERT INTO phone(emp_id,type,number,ext)
  VALUES (2,'wrk',33333333,45);
--Foreign Key
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products (
    product_no integer PRIMARY KEY,
    name text,
    price numeric
);
DROP TABLE IF EXISTS orders CASCADE;
CREATE TABLE orders (
    order_id integer PRIMARY KEY,
    product_no integer REFERENCES products (product_no),
    quantity integer
);
DROP TABLE IF EXISTS orders CASCADE;
CREATE TABLE orders (
    order_id integer PRIMARY KEY,
    product_no integer REFERENCES products,
    quantity integer
);
DROP TABLE IF EXISTS t1 CASCADE;
CREATE TABLE t1 (
    a integer,
    b integer,
    c integer,
    PRIMARY KEY (a, b),
    FOREIGN KEY (b, c) REFERENCES t1 (a, b)
);
DROP TABLE IF EXISTS tree CASCADE;
CREATE TABLE tree (
    node_id integer PRIMARY KEY,
    parent_id integer REFERENCES tree(node_id),
    name text
);
\echo --Multiple Foreign Keys
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products (
    product_no integer PRIMARY KEY,
    name text,
    price numeric
);
DROP TABLE IF EXISTS orders CASCADE;
CREATE TABLE orders (
    order_id integer PRIMARY KEY,
    shipping_address text
);
DROP TABLE IF EXISTS order_items CASCADE;
CREATE TABLE order_items (
    product_no integer REFERENCES products,
    order_id integer REFERENCES orders,
    quantity integer,
    PRIMARY KEY (product_no, order_id)
);
\echo --Many-to-many relationship
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products (
    product_no integer PRIMARY KEY,
    name text,
    price numeric
);
DROP TABLE IF EXISTS orders CASCADE;
CREATE TABLE orders (
    order_id integer PRIMARY KEY,
    shipping_address text
);
DROP TABLE IF EXISTS order_items CASCADE;
CREATE TABLE order_items (
    product_no integer REFERENCES products ON DELETE RESTRICT,
    order_id integer REFERENCES orders ON DELETE CASCADE,
    quantity integer,
    PRIMARY KEY (product_no, order_id)
);
\echo --System-columns
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    name varchar(20)
);
INSERT INTO t VALUES ('Alain'),('Eddy');
SELECT tableoid,ctid FROM t;

\echo --Cleanup
DROP TABLE IF EXISTS myuser CASCADE;
DROP TABLE IF EXISTS my_first_table CASCADE;
DROP TABLE IF EXISTS people CASCADE;
DROP TABLE IF EXISTS example CASCADE;
DROP TABLE IF EXISTS person CASCADE;
DROP TABLE IF EXISTS owner CASCADE;
DROP TABLE IF EXISTS car CASCADE;
DROP TABLE IF EXISTS phone CASCADE;
DROP TABLE IF EXISTS t1 CASCADE;
DROP TABLE IF EXISTS tree CASCADE;
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE IF EXISTS order_items CASCADE;
DROP TABLE IF EXISTS orders CASCADE;
DROP TABLE IF EXISTS t CASCADE;

