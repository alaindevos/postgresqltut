CREATE LANGUAGE plpython3u;
DROP FUNCTION IF EXISTS pymax(a integer,b integer);
CREATE FUNCTION pymax(a integer,b integer)
    RETURNS integer
AS $$
    if a>b:
        return a
    return b
$$ LANGUAGE plpython3u;
SELECT pymax(3,5);

SELECT * FROM pg_available_extensions ORDER BY 1;

DROP FUNCTION IF EXISTS pystrip(x text);
CREATE FUNCTION pystrip(x text)
    RETURNS text
AS $$
    global x
    x=x.strip()
    return x
$$ LANGUAGE plpython3u;
SELECT pystrip('  123  ');


