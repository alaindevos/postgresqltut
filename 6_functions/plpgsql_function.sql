--Functions
DROP TABLE IF EXISTS myint CASCADE;
CREATE TABLE IF NOT EXISTS myint(
x int
);
INSERT INTO myint VALUES (1);
INSERT INTO myint VALUES (2);
INSERT INTO myint VALUES (3);
CREATE OR REPLACE FUNCTION myloop() RETURNS int AS
$$
DECLARE s int:=0;
DECLARE row record;
BEGIN
  FOR row in 
    EXECUTE 'SELECT * FROM myint'
  LOOP
    s:=s+row.x;
  END LOOP;
INSERT INTO myint VALUES (123);
RETURN s;
END;
$$LANGUAGE plpgsql;
SELECT myloop();
SELECT myloop();

--Functions
DROP FUNCTION IF EXISTS returnints(n int);
CREATE OR REPLACE FUNCTION returnints(n int) RETURNS setof myint AS
$$
DECLARE i int:=0;
BEGIN
DROP TABLE IF EXISTS tmp;
CREATE TABLE IF NOT EXISTS tmp(LIKE myint);
  LOOP
       i:=i+1;
       INSERT INTO tmp(x) VALUES (i);
       EXIT WHEN i=n;
  END LOOP;
RETURN QUERY SELECT * FROM tmp;
END;
$$LANGUAGE plpgsql;
SELECT * FROM returnints(3);
SELECT * FROM myint;

--Functions
DROP FUNCTION IF EXISTS myset(int);
CREATE OR REPLACE FUNCTION myset(a int)
RETURNS SETOF int AS
$$
DECLARE
  i int:=0;
BEGIN
  LOOP
    i:=i+1;
    RETURN NEXT i;
    EXIT WHEN i=a;
  END LOOP;
END
$$ LANGUAGE plpgsql;
SELECT myset(3);
SELECT * FROM myset(3);

--Functions
DROP TABLE IF EXISTS myint2 CASCADE;
CREATE TABLE IF NOT EXISTS myint2(
x int
);
INSERT INTO myint2 VALUES (1);
INSERT INTO myint2 VALUES (2);
INSERT INTO myint2 VALUES (3);
CREATE OR REPLACE FUNCTION getmyint2()
RETURNS SETOF myint2 AS
$$
BEGIN
  RETURN QUERY SELECT * FROM myint2;
END;
$$ LANGUAGE plpgsql;
SELECT * FROM getmyint2();

--Functions
DROP FUNCTION IF EXISTS myswap(INOUT int,INOUT int);
CREATE OR REPLACE FUNCTION myswap(INOUT x int,INOUT y int) AS
$$
DECLARE z int;
BEGIN
  z=x;
  x=y;
  y=z;
END
$$ LANGUAGE plpgsql;
SELECT * FROM myswap(3,5);

--Functions
DROP FUNCTION IF EXISTS myfour(OUT int,OUT int);
CREATE OR REPLACE FUNCTION myfour(OUT a int,OUT b int)
RETURNS SETOF RECORD AS
$$
BEGIN
  SELECT 1,1 INTO  a,b;
  RETURN NEXT;
  SELECT 1,2 INTO  a,b;
  RETURN NEXT;
  SELECT 2,1 INTO  a,b;
  RETURN NEXT;
  SELECT 2,2 INTO  a,b;
  RETURN NEXT;
END
$$ LANGUAGE plpgsql;
SELECT * FROM myfour();

--Functions
DROP TABLE IF EXISTS m CASCADE;
CREATE TABLE m(
account_id SERIAL PRIMARY KEY,
x int NOT NULL
);
INSERT INTO m(x) VALUES (1);
INSERT INTO m(x) VALUES (2);
INSERT INTO m(x) VALUES (3);

DROP FUNCTION IF EXISTS mytwo();
CREATE OR REPLACE FUNCTION mytwo() RETURNS SETOF JSON AS
$$BEGIN
RETURN QUERY SELECT row_to_json(m) FROM m WHERE x=2;
END;$$ LANGUAGE plpgsql;
SELECT * FROM mytwo();

DROP FUNCTION IF EXISTS myall();
CREATE OR REPLACE FUNCTION myall() RETURNS SETOF int AS
$$BEGIN
RETURN QUERY SELECT m.x FROM m;
END;$$ LANGUAGE plpgsql;
SELECT * from myall();

DROP FUNCTION IF EXISTS myall2();
CREATE OR REPLACE FUNCTION myall2() RETURNS SETOF m AS
$$BEGIN
RETURN QUERY SELECT * FROM m;
END;$$ LANGUAGE plpgsql;
SELECT * from myall2();
SELECT * INTO m2 FROM m;
SELECT * from m2;

--Datatypes
DROP TABLE t CASCADE;
CREATE TABLE t(
doc jsonb
);
INSERT INTO t(doc) VALUES ('{"a":1,"b":2}');
INSERT INTO t(doc) VALUES ('{"c":3,"d":4}');
SELECT doc from t;
SELECT doc->>'a' from t;
SELECT jsonb_each(doc) from t;
SELECT doc from t WHERE doc @> '{"a":1}';

--Datatypes
DROP TABLE IF EXISTS m CASCADE;
CREATE TABLE m(
x int,
y int
);
INSERT INTO m(x,y) VALUES (1,2);
INSERT INTO m(x,y) VALUES (3,4);
\echo select
SELECT (x,y) FROM m;
\echo select row to json
SELECT row_to_json(row(x,y)) FROM m;

--Functions
DROP TABLE IF EXISTS name CASCADE; 
CREATE TABLE name(
first varchar,
last varchar
);

DROP FUNCTION IF EXISTS modifylast(varchar,varchar);
CREATE FUNCTION modifylast(f varchar,l varchar) RETURNS void AS
$$BEGIN
  UPDATE name SET last=l WHERE first=f; 
END$$
language plpgsql;

INSERT INTO name(first,last) VALUES ('Jan','DeWolf');
INSERT INTO name(first,last) VALUES ('Piet','DeVos');
SELECT * from name;
SELECT modifylast('Piet'::varchar,'Janssens'::varchar);
SELECT * from name;

--cte-where
DROP TABLE n CASCADE;
CREATE TABLE n
(
name varchar
);
INSERT INTO n(name) VALUES ('a');
INSERT INTO n(name) VALUES ('b');
SELECT name AS "mynames" from n;
SELECT * from n AS n1,n AS n2;
SELECT n1.name AS "n1_name", n2.name AS "n2_name" from n AS n1,n AS n2
  WHERE n1.name=n2.name;

--datatypes
\echo truncate
\echo drop m
DROP TABLE m CASCADE;
\echo drop mt
DROP TABLE mt CASCADE;
\echo create m
CREATE TABLE m(
x int
);
\echo create mt
CREATE TABLE mt(LIKE m);
ALTER TABLE mt
  ADD COLUMN t TIMESTAMP WITHOUT TIME ZONE NOT NULL;
  CREATE RULE mytimestamp AS
    ON INSERT TO m DO ALSO
      INSERT INTO mt(x,t) VALUES (new.x,now());
\echo insert into m
INSERT INTO m(x) VALUES (1);
INSERT INTO m(x) VALUES (2);
\echo select from mt
SELECT * FROM mt;

--Function
DROP TABLE IF EXISTS myint CASCADE;
CREATE TABLE iF NOT EXISTS myint(
x int
);
INSERT INTO myint VALUES (0);
INSERT INTO myint VALUES (1);
SELECT table_to_xml('myint',true,false,'') AS s;

SELECT array_to_json(array[0,1]);
SELECT row_to_json(mi) FROM myint mi;

DROP FUNCTION IF EXISTS mysum(int[]);
CREATE FUNCTION mysum(a int[]) RETURNS int as
$$
DECLARE s int:=0;
DECLARE X int:=0;
BEGIN
  FOREACH x IN ARRAY a
  LOOP
    s:=s+x;
  END LOOP;
  RETURN s;
END;
$$ LANGUAGE plpgsql;
SELECT mysum(ARRAY[1,2,3]);


DROP FUNCTION IF EXISTS addone(int);
CREATE FUNCTION addone(int)
RETURNS int AS
$$ BEGIN
  RETURN $1+1;
END $$
LANGUAGE 'plpgsql';
SELECT addone(2) as answer;

DROP FUNCTION IF EXISTS mid1(varchar,integer,integer);
CREATE FUNCTION mid1(varchar,integer,integer) RETURNS varchar AS
$$BEGIN
  --String,begin,length
  RETURN substring($1,$2,$3);
END;$$
LANGUAGE plpgsql;
SELECT mid1('123456789',2,5);

DROP FUNCTION IF EXISTS mid2(s varchar,b int,l int);
CREATE OR REPLACE FUNCTION mid2(s varchar,b int,l int) RETURNS varchar AS
$$BEGIN
  --String,begin,length
  RETURN substring(s,b,l);
END;$$
LANGUAGE plpgsql;
SELECT mid2('123456789',2,5);

DROP FUNCTION IF EXISTS isfive(int);
CREATE OR REPLACE FUNCTION isfive(i int) RETURNS bool AS
$$BEGIN
  IF i=5 then
    RETURN true;
  ELSE
    RETURN false;
  END IF;
END$$
LANGUAGE plpgsql;
SELECT isfive(5);
SELECT isfive(3);

DROP FUNCTION IF EXISTS mysum(int);
CREATE OR REPLACE FUNCTION mysum(n int) RETURNS int AS
$$
DECLARE s int:=0;
DECLARE i int:=0;
BEGIN
  LOOP
       i:=i+1;
       s:=s+i;
       EXIT WHEN i=n;
  END LOOP;
RETURN s;
END;
$$LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS mysum2(int);
CREATE OR REPLACE FUNCTION mysum2(n int) RETURNS int AS
$$
DECLARE s int:=0;
DECLARE i int:=0;
BEGIN
  WHILE i<n+1 LOOP
       i:=i+1;
       s:=s+i;
  END LOOP;
RETURN s;
END;
$$LANGUAGE plpgsql;
\echo mysum 4
SELECT mysum(4);
\echo mysum2 4
SELECT mysum2(4);

\q
DROP FUNCTION myconcat(text,text);
CREATE FUNCTION myconcat(a text,b text) RETURNS text
AS
$$
SELECT LOWER($1 || $2) ;
$$
LANGUAGE SQL IMMUTABLE STRICT;

SELECT myconcat('aaa','bbb');
SELECT myconcat( a=> 'aaa', b=> 'bbb');
CREATE TABLE foo(fooid INT, foosubid INT,fooname TEXT);
INSERT INTO foo VALUES(1,2,'three');
INSERT INTO foo VALUES(4,5,'six');

CREATE OR REPLACE FUNCTION getallfoo() RETURNS SETOF foo AS
$$
DECLARE
  r foo%rowtype;
BEGIN
  FOR r IN
    SELECT * FROM foo
  LOOP
    RETURN NEXT r;
  END LOOP;
  RETURN;
END
$$
LANGUAGE plpgsql;

SELECT * FROM getallfoo();


CREATE OR REPLACE FUNCTION getallfoo2() RETURNS SETOF int AS
$$
BEGIN
  RETURN QUERY SELECT fooid FROM foo;
  RETURN;
END
$$
LANGUAGE plpgsql;

SELECT * FROM getallfoo2();
