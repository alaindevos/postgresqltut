CREATE TABLE emp(
    id serial,
    salary integer
);
INSERT INTO emp(salary) VALUES (3),(-3),(5),(-5);

DROP FUNCTION clean_emp();
CREATE FUNCTION clean_emp() RETURNS void AS
'
    DELETE FROM emp
        WHERE salary <0;
' LANGUAGE SQL;

SELECT clean_emp();
SELECT * FROM emp;


