\echo -- Select Items
DROP TABLE IF EXISTS  t CASCADE;
CREATE TABLE t(
    a int,
    b int);
INSERT INTO t VALUES (1,2),(3,4);
SELECT a,b FROM t;
SELECT t.a,t.b FROM t;
SELECT t2.a,t2.b FROM t AS t2;
\echo -- Column labels
SELECT 1 AS a;
\echo -- Distinct
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    i INT
);
INSERT INTO t(i) VALUES (1),(2),(1);
SELECT 
    DISTINCT i 
        FROM t ORDER BY i;
\echo -- UNION,INTERSECT,EXCEPT
DROP TABLE IF EXISTS t1 CASCADE;
CREATE TABLE t1(
    a int);
INSERT INTO t1 VALUES (1),(2),(3);
DROP TABLE IF EXISTS t2 CASCADE;
CREATE TABLE t2(
    b int);
INSERT INTO t2 VALUES (2),(3),(4);
SELECT * FROM t1
    UNION SELECT * FROM t2;
SELECT * FROM t1
    INTERSECT SELECT * FROM t2;
SELECT * FROM t1
    EXCEPT SELECT * FROM t2;
\echo -- Sort
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    i INT
);
INSERT INTO t(i) VALUES (2),(1);
SELECT * FROM t ORDER BY i;
\echo -- Order by index
SELECT * FROM t ORDER BY 1;
\echo -- DESC ASC
SELECT * FROM t ORDER BY i ASC;
SELECT * FROM t ORDER BY i DESC;
\echo -- Limit,Offset
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    i INT
);
INSERT INTO t(i) VALUES (1),(2),(3),(4),(5);
SELECT * FROM t LIMIT 3;
SELECT * FROM t OFFSET 3;
SELECT * FROM t LIMIT 2 OFFSET 2;
\echo -- Constant table
VALUES (1,'one'),(2,'two');

SELECT 1 AS column1, 'one' AS column2
    UNION ALL
SELECT 2, 'two';

\echo -- CTE & WITH
DROP TABLE IF EXISTS m CASCADE;
CREATE TABLE m(
x int,
y int
);


INSERT INTO m VALUES (1,1);
INSERT INTO m VALUES (2,2);
INSERT INTO m VALUES (3,3);

WITH w AS
  (SELECT * FROM m)
    SELECT * FROM w WHERE x=2;

SELECT * FROM m WHERE x=2;

WITH w AS NOT MATERIALIZED
  (SELECT * FROM m)
    SELECT * FROM w AS w1
      JOIN w AS w2
        ON w1.x=w2.x
          WHERE w2.y=2;

WITH source AS
  (SELECT 1)
    SELECT * FROM source;

WITH source AS
  (SELECT 1 AS col1)
    SELECT * FROM source;

WITH source(col1) AS
  (SELECT 1)
    SELECT * FROM source;

WITH source(col2) AS
  (SELECT 1 AS col1)
    SELECT col2 AS col3 FROM source;

WITH source AS
  (SELECT (1,2,3) )
SELECT * FROM source;

WITH source AS
  (SELECT (1,2,3) )
    SELECT * FROM source;

SELECT * FROM
  (SELECT 1) AS source;

DROP TABLE IF EXISTS demo CASCADE;
CREATE TABLE demo(
  x1 NUMERIC,
  x2 NUMERIC);
WITH source AS
  (INSERT INTO demo VALUES(random(),random())
   RETURNING x1,x2
  )
SELECT x1,x2 FROM source;

DROP TABLE IF EXISTS employee CASCADE;
CREATE TABLE employee
  (
    id serial primary key,
    name VARCHAR(150) NOT NULL,
    title VARCHAR(100),
    office VARCHAR(100)
  );

INSERT INTO employee VALUES (1,'Alain','Manager','Brussels');
INSERT INTO employee VALUES (2,'Frans','Worker','Brussels');
INSERT INTO employee VALUES (3,'Eddy','Worker','Aalst');
select * from employee;

WITH emp_brussels AS
  (SELECT * FROM employee WHERE office='Brussels')
    SELECT * FROM emp_brussels WHERE title !='Manager' ORDER BY title;

SELECT * FROM
  (SELECT * FROM employee WHERE office='Brussels')
    AS emp_brussels
      WHERE title !='Manager' ORDER BY title;

WITH emp_brussels AS
  (SELECT * FROM employee WHERE office='Brussels'),
     emp_brussels_worker AS
       (SELECT * FROM emp_brussels WHERE title='Worker')
          SELECT * FROM emp_brussels_worker;

\echo -- Complex query
DROP TABLE IF EXISTS commission CASCADE;
CREATE TABLE commission
  (
   id serial primary key,
   salesperson_id INT  NOT NULL,
   commission_id  INT  NOT NULL,
   commission_amount DECIMAL(12,2) NOT NULL,
   commission_date DATE NOT NULL
  );

INSERT INTO commission VALUES (1,1,1,1000,'2020-10-10');
INSERT INTO commission VALUES (2,1,2,2000,'2020-11-10');
INSERT INTO commission VALUES (3,2,3,1500,'2020-11-10');
INSERT INTO commission VALUES (4,1,4,3000,'2021-10-10');
INSERT INTO commission VALUES (5,1,5,4000,'2021-11-10');
INSERT INTO commission VALUES (6,2,6,5500,'2021-11-10');

SELECT
  salesperson_id,
  EXTRACT(YEAR FROM commission_date) AS year,
  SUM(commission_amount) AS total
FROM
  commission
GROUP BY
  salesperson_id,year;


WITH commission_year AS
  (SELECT
      salesperson_id,
      EXTRACT(YEAR FROM commission_date) AS year,
      SUM(commission_amount) AS total
   FROM
      commission
   GROUP BY
      salesperson_id,year
  )

SELECT *
FROM
  commission_year CUR,
  commission_year PREV
WHERE
  CUR.salesperson_id=PREV.salesperson_id
AND
  CUR.year=PREV.year+1;
\echo -- Data modifying width
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products(
  id int);
INSERT INTO products VALUES (1),(2),(3);
DROP TABLE IF EXISTS products_log CASCADE;
CREATE TABLE products_log(
  id int);
WITH moved_rows AS
  (DELETE FROM products
    RETURNING *
  )
    INSERT INTO products_log
      SELECT * FROM moved_rows;

\echo -- Alias
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    i INT
);
INSERT INTO t(i) VALUES (0),(1),(2),(3);
SELECT alias.i FROM t AS "alias";


\echo -- Insert into
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    i INT
);
INSERT INTO t(i) VALUES (0),(1),(2),(3),(4);
SELECT * FROM t 
    WHERE i 
        BETWEEN 1 AND 3;

\echo -- Null
DROP TABLE IF EXISTS d CASCADE;
CREATE TABLE d(
    description VARCHAR,
    short_description VARCHAR
);
INSERT INTO d VALUES (Null,Null);
INSERT INTO d VALUES (Null,'Short');
INSERT INTO d VALUES ('Long description','NULL');
INSERT INTO d VALUES ('Very Long description','Also Short');
SELECT * FROM d;
\echo -- Coalescence return first not null value
select coalesce(description,short_description,'UNKNOWN') from d;
\echo -- ?????
DROP TABLE IF EXISTS customer CASCADE;
CREATE TABLE customer(
    id INT,
    firstname TEXT,
    lastname TEXT
);
INSERT INTO customer VALUES (1,'Alain','DeVos'),(2,'Eddy','DeWolf');
SELECT * FROM customer
  WHERE id IN
    (SELECT id FROM customer WHERE lastname='DeVos');

\echo -- Not null    
DROP TABLE IF EXISTS tt CASCADE;
CREATE TABLE tt(
  t TEXT
);
INSERT INTO tt(t) VALUES ('Alain'),('alain'),('ALAIN'),('aLain');
SELECT * FROM tt WHERE t LIKE '_l%';
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    i INT
);
INSERT INTO t(i) VALUES (1),(NULL),(2),(NULL);
SELECT  * FROM t;
SELECT  * FROM t WHERE i IS NOT NULL;

\echo -- nullif returns null if the two arguments are the same, otherwise the first argument
DROP TABLE IF EXISTS d CASCADE;
CREATE TABLE d(
    i INTEGER
);
INSERT INTO d VALUES (0),(1),(2),(3);
SELECT NULLIF(i,2) FROM d;

\echo -- Cleanup
DROP TABLE IF EXISTS commission CASCADE;
DROP TABLE IF EXISTS customer CASCADE;
DROP TABLE IF EXISTS d CASCADE;
DROP TABLE IF EXISTS demo CASCADE;
DROP TABLE IF EXISTS employee CASCADE;
DROP TABLE IF EXISTS m CASCADE;
DROP TABLE IF EXISTS products CASCADE;
DROP TABLE IF EXISTS products_log CASCADE;
DROP TABLE IF EXISTS t CASCADE;
DROP TABLE IF EXISTS t1 CASCADE;
DROP TABLE IF EXISTS t2 CASCADE;
DROP TABLE IF EXISTS tt CASCADE;

