\echo -- SELECT
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    i INT
);
INSERT INTO t(i) VALUES (2),(1);
SELECT * FROM t;

\echo -- JOIN1
DROP TABLE IF EXISTS book CASCADE;
CREATE TABLE book (book_id INT,title TEXT,status INT);
INSERT INTO book VALUES(100,'Booka',0);
INSERT INTO book VALUES(101,'Bookb',1);
INSERT INTO book VALUES(102,'Bookc',0);

DROP TABLE IF EXISTS status_name CASCADE;
CREATE TABLE status_name(status_id INT,status_name CHAR(8));
INSERT INTO status_name VALUES (0,'Inactive'),(1,'Active');
SELECT * FROM status_name;

SELECT book_id,title,status_name
FROM book INNER JOIN status_name
  ON book.status=status_name.status_id;

\echo -- JOIN2
DROP TABLE IF EXISTS phone CASCADE;
DROP TABLE IF EXISTS person CASCADE;
CREATE TABLE person(
  id SERIAL,
  emp_id int,
  name VARCHAR(100),
  --
  PRIMARY KEY (id),
  UNIQUE(id)
);
INSERT INTO person(emp_id,name) VALUES (1,'Alain');
INSERT INTO person(emp_id,name) VALUES (2,'Eddy');
INSERT INTO person(emp_id,name) VALUES (3,'Alain');

DROP TABLE IF EXISTS phone CASCADE;
CREATE TABLE phone (
  id SERIAL,
  emp_id int,
  type char(3),
  number bigint,
  ext int,
  --
  PRIMARY KEY (id),
  UNIQUE(id),
  FOREIGN KEY(emp_id) REFERENCES person(id)
);
INSERT INTO phone(emp_id,type,number,ext)
  VALUES (1,'wrk',11111111,23);
INSERT INTO phone(emp_id,type,number,ext)
  VALUES (1,'hom',22222222,NULL);
INSERT INTO phone(emp_id,type,number,ext)
  VALUES (2,'wrk',33333333,45);
  
\echo -- Inner Join
SELECT name,type,number,ext
  FROM person p INNER JOIN phone f
    ON p.emp_id = f.emp_id;
DROP TABLE IF EXISTS employee CASCADE;

CREATE TABLE employee (
	employee_id INT PRIMARY KEY,
	first_name VARCHAR (255) NOT NULL,
	last_name VARCHAR (255) NOT NULL,
	manager_id INT,
	FOREIGN KEY (manager_id) 
	REFERENCES employee (employee_id) 
	ON DELETE CASCADE
);

INSERT INTO employee (
	employee_id,
	first_name,
	last_name,
	manager_id
)
VALUES
	(1, 'Windy', 'Hays', NULL),
	(2, 'Ava', 'Christensen', 1),
	(3, 'Hassan', 'Conner', 1),
	(4, 'Anna', 'Reeves', 2),
	(5, 'Sau', 'Norman', 2),
	(6, 'Kelsie', 'Hays', 3),
	(7, 'Tory', 'Goff', 3),
	(8, 'Salley', 'Lester', 3);

SELECT
    e.first_name || ' ' || e.last_name employee,
    m .first_name || ' ' || m .last_name manager
FROM
    employee e
        INNER JOIN employee m 
            ON m .employee_id = e.manager_id
                ORDER BY manager;
\echo -- Join3
DROP TABLE IF EXISTS t1 CASCADE;
CREATE table t1(
    num int,
    name varchar(20)

);
INSERT INTO t1 VALUES (1,'a');
INSERT INTO t1 VALUES (2,'b');
INSERT INTO t1 VALUES (3,'c');
DROP TABLE IF EXISTS t2 CASCADE;
CREATE table t2(
    num int,
    value varchar(20)

);
INSERT INTO t2 VALUES (1,'xxx');
INSERT INTO t2 VALUES (3,'yyy');
INSERT INTO t2 VALUES (5,'zzz');
SELECT * FROM t1 CROSS JOIN t2;
SELECT * FROM t1 INNER JOIN t2
    ON t1.num=t2.num;
SELECT * FROM t1 INNER JOIN t2
    USING (num);
SELECT * FROM t1 NATURAL INNER JOIN t2;
SELECT * FROM t1 LEFT JOIN t2 
    ON t1.num = t2.num;
SELECT * FROM t1 LEFT JOIN t2 
    USING (num);
SELECT * FROM t1 RIGHT JOIN t2 
    ON t1.num = t2.num;
SELECT * FROM t1 FULL JOIN t2 
    ON t1.num = t2.num;
SELECT * FROM t1 LEFT JOIN t2 
    ON t1.num = t2.num 
        AND t2.value = 'xxx';
SELECT * FROM t1 LEFT JOIN t2 
    ON t1.num = t2.num 
        WHERE t2.value = 'xxx';
\echo -- Table Alias
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    i INT
);
INSERT INTO t(i) VALUES (2),(1);
SELECT mytable.i FROM t AS mytable;
\echo -- Column Alias
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    i INT
);
INSERT INTO t(i) VALUES (2),(1);
SELECT i AS "mycolumn" FROM t;
\echo -- In subquery
DROP TABLE IF EXISTS c CASCADE;
CREATE TABLE c(
    v INT,
    n TEXT
);
INSERT INTO c(v,n) VALUES (1,'Alain'),(2,'Eddy'),(3,'Alain'),(4,'Jan');
SELECT * FROM c;
SELECT * FROM c
    WHERE n IN ('Alain','Eddy');
SELECT * FROM c
    WHERE n IN 
        (SELECT n FROM c WHERE v=1);
\echo -- Subquery
SELECT mytable.n FROM 
    (SELECT * FROM c) AS mytable;
\echo -- Table function
DROP TABLE IF EXISTS foo CASCADE;
CREATE TABLE foo (
    fooid int, 
    foosubid int, 
    fooname text
);
INSERT INTO foo VALUES (1,2,'one');
INSERT INTO foo VALUES (3,4,'tree');
DROP FUNCTION IF EXISTS getfoo(int) CASCADE;
CREATE FUNCTION getfoo(int) RETURNS SETOF foo AS $$
    SELECT * FROM foo WHERE fooid = $1;
    $$ LANGUAGE SQL;
SELECT * FROM getfoo(1) AS t1;
SELECT * FROM foo 
    WHERE foosubid IN 
        (SELECT foosubid FROM getfoo(foo.fooid) z 
            WHERE z.fooid = foo.fooid);
CREATE VIEW vw_getfoo AS 
    SELECT * FROM getfoo(1);
SELECT * FROM vw_getfoo;
\echo -- Select Rows
SELECT * FROM ROWS FROM
    (json_to_recordset('[{"a":40,"b":"foo"},{"a":"100","b":"bar"}]')
        AS (a INTEGER, b TEXT), generate_series(1, 3) ) AS x (p, q, s)
            ORDER BY p;
\echo -- WHERE
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    i INT
);
INSERT INTO t(i) VALUES (1),(2),(1);
SELECT i FROM t WHERE i=1;
\echo -- IN
SELECT i FROM t WHERE i in (1,2,3);
SELECT i FROM t WHERE i in (SELECT 1);
\echo -- BETWEEN
SELECT i FROM t WHERE i BETWEEN 1 AND 3;
\echo -- exists
SELECT i FROM t WHERE EXISTS
    (SELECT i FROM t);
\echo -- GROUP BY
DROP TABLE IF EXISTS p CASCADE;
CREATE TABLE p (
    amount INT,
    customer TEXT
);
INSERT INTO p(amount,customer)  VALUES  (100,'Alain'),(200,'Eddy'),(300,'Alain');
SELECT
    customer,
    sum(amount)
FROM p
    GROUP BY customer;
\echo --HAVING
DROP TABLE IF EXISTS p CASCADE;
CREATE TABLE p (
    amount INT,
    customer TEXT
);
INSERT INTO p(amount,customer)  VALUES  (100,'Alain'),(200,'Eddy'),(300,'Alain');
SELECT
    customer,
    sum(amount)
FROM p
    GROUP BY customer
HAVING
    sum(amount) > 200;

\echo -- GROUPING SET
DROP TABLE IF EXISTS items_sold CASCADE;
CREATE TABLE items_sold(
    brand varchar(20),
    size  varchar(1),
    sales int
);
INSERT INTO items_sold VALUES ('Foo','L',10);
INSERT INTO items_sold VALUES ('Foo','M',20);
INSERT INTO items_sold VALUES ('Bar','M',15);
INSERT INTO items_sold VALUES ('Bar','L',5);
SELECT brand, size, sum(sales) FROM items_sold 
    GROUP BY GROUPING SETS ((brand), (size), ());

\echo -- Cleanup
DROP TABLE IF EXISTS book CASCADE;
DROP TABLE IF EXISTS c CASCADE;
DROP TABLE IF EXISTS employee CASCADE;
DROP TABLE IF EXISTS foo CASCADE;
DROP TABLE IF EXISTS items_sold CASCADE;
DROP TABLE IF EXISTS p CASCADE;
DROP TABLE IF EXISTS person CASCADE;
DROP TABLE IF EXISTS phone CASCADE;
DROP TABLE IF EXISTS status_name CASCADE;
DROP TABLE IF EXISTS t CASCADE;
DROP TABLE IF EXISTS t1 CASCADE;
DROP TABLE IF EXISTS t2 CASCADE;
DROP VIEW IF EXISTS vw_getfoo CASCADE;

