--Index
DROP TABLE IF EXISTS bird_order CASCADE;
CREATE TABLE bird_order (
  order_id SERIAL PRIMARY KEY,
  name VARCHAR(255) UNIQUE,
  description VARCHAR(255)
);
CREATE INDEX order_id_index ON bird_order(order_id);

INSERT INTO bird_order(name,description) VALUES ('Papegaai','Een papegaai'),('Merel','Een merel');
SELECT * FROM bird_order;

\echo -- CREATE UNIQUE INDEX t_pkey ON public.t USING btree (i) 
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    i INTEGER
);
ALTER TABLE t ADD PRIMARY KEY (i);
\d t

\echo -- Multicolumn index
CREATE TABLE test2 (
  major int,
  minor int,
  name varchar
);
CREATE INDEX test2_mm_idx ON test2 (major, minor);


\echo -- Idem , unique index
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    i INTEGER
);
CREATE UNIQUE INDEX t_pkey ON public.t USING btree (i);

\echo -- Index on expression
DROP TABLE IF EXISTS t CASCADE;
CREATE TABLE t(
    s text
);
CREATE UNIQUE INDEX t_pkey ON public.t USING btree (lower(s));
INSERT INTO t(s) VALUES ('AAA');
INSERT INTO t(s) VALUES ('aaa');
\echo -- cleanup
DROP TABLE IF EXISTS bird_order CASCADE;
DROP TABLE IF EXISTS t CASCADE;
DROP TABLE IF EXISTS test2 CASCADE;
\q
