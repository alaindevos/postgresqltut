\echo -- Insert Intro
DROP TABLE IF EXISTS t1 CASCADE;
CREATE TABLE t1(
  i INT
);
INSERT INTO t1(i) VALUES (1),(2);
DROP TABLE IF EXISTS t2 CASCADE;
CREATE TABLE t2 ( LIKE t1);
SELECT * FROM t2;
INSERT INTO t2
  SELECT * FROM t1;
SELECT * FROM t2;
\echo -- Insert
DROP TABLE IF EXISTS products CASCADE;
CREATE TABLE products (
    product_no integer,
    name text,
    price numeric
);
INSERT INTO products VALUES (1,'Cheese',9.99);
\echo -- Insert List Columns
INSERT INTO products (product_no, name, price) VALUES (1, 'Cheese',9.99);
INSERT INTO products (name, price, product_no) VALUES ('Cheese',9.99, 1);
\echo -- Insert Default Values
INSERT INTO products (product_no, name, price) VALUES (1, 'Cheese', DEFAULT);
INSERT INTO products DEFAULT VALUES;
\echo -- Insert multiple rows
INSERT INTO products (product_no, name, price) VALUES
    (1, 'Cheese', 9.99),
    (2, 'Bread', 1.99),
    (3, 'Milk', 2.99);
\echo -- Insert result of a query
DROP TABLE IF EXISTS new_products CASCADE;
CREATE TABLE new_products (
    product_no integer,
    name text,
    price numeric
);
INSERT INTO new_products (product_no, name, price) VALUES
    (4, 'Cheese', 9.99),
    (5, 'Bread', 1.99),
    (6, 'Milk', 2.99);
INSERT INTO products (product_no, name, price)
    SELECT product_no, name, price FROM new_products;
\echo -- Updating
UPDATE products SET price = 10 WHERE price = 9.99;
UPDATE products SET price = price * 1.10;
UPDATE products SET price = 10 , name = 'milk', product_no=77 WHERE price = 11.0;
\echo -- Delete
DELETE FROM products WHERE price = 10;
DELETE FROM products;
\echo -- Truncate
TRUNCATE products;
\echo -- Return modified or deleted
DROP TABLE products;
CREATE TABLE products (
    product_no integer,
    name text,
    price numeric
);
INSERT INTO products (product_no, name, price) VALUES
    (4, 'Cheese', 9.99),
    (5, 'Bread', 9.99),
    (6, 'Milk', 2.99);
UPDATE products SET price = price * 1.10
    WHERE price = 9.99
        RETURNING name, price AS new_price;
DELETE FROM products
    WHERE price = 10.9890
        RETURNING *;
\echo -- Cleanup
DROP TABLE IF EXISTS t1;
DROP TABLE IF EXISTS t2;
DROP TABLE IF EXISTS new_products;
DROP TABLE IF EXISTS products;


