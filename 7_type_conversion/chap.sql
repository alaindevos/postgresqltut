SELECT '100'::INTEGER;
SELECT '01-OCT-2015'::DATE;
SELECT CAST('100' AS INTEGER);
SELECT CAST('2015-01-01' AS DATE);
SELECT CAST('10.2' AS DOUBLE PRECISION);
SELECT CAST('T' AS BOOLEAN);
SELECT '2019-06-15 14:30:20'::timestamp;
SELECT '15 minute'::interval;
SELECT CAST (40 AS bigint) ! AS "40 factorial";
SELECT 'abc' || 'def' AS "unspecified";
SELECT round(4, 4);
SELECT round(CAST (4 AS numeric), 4);
SELECT round(4.0, 4);
SELECT substr('1234', 3);
SELECT substr(varchar '1234', 3);
SELECT substr(CAST (varchar '1234' AS text), 3);
SELECT substr(CAST (1234 AS text), 3);
DROP TABLE IF EXISTS vv CASCADE;
CREATE TABLE vv (v character(20));
INSERT INTO vv SELECT 'abc' || 'def';
SELECT v, octet_length(v) FROM vv;
SELECT text 'a' AS "text" UNION SELECT 'b';
SELECT 1.2 AS "numeric" UNION SELECT 1;
SELECT 1 AS "real" UNION SELECT CAST('2.2' AS REAL);

\echo -- Cleanup
DROP TABLE IF EXISTS vv CASCADE;

